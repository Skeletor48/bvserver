import express from "express";
import routes from "./src/routes/bvsRoutes";
import bodyParser from "body-parser";
import apicache from "apicache";
import session from "express-session";
import cookieParser from "cookie-parser";
import i18n from "i18n";

const app = express();
const PORT = process.env.PORT || 4000;

let cache = apicache.middleware;
app.use(cache("2 minutes"));

i18n.configure({
  locales: ["en", "zh", "de"],
  directory: __dirname + "/locales",
  defaultLocale: "en",
  cookie: "i18n",
});

app.use(cookieParser("bvserver"));

app.use(
  session({
    secret: "bvserver",
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 60000 },
  })
);

app.use(i18n.init);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

app.get('/',(req,res)=>{
  res.send(`app is up and runnning`);
})

app.listen(PORT, (error) => {
  if (error) {
    throw error;
  }
  console.log(`Server is running on  ${PORT}`);
});

process.on("uncaughtException", (err) => {
  console.error("There was an uncaught error", err);
  process.exit(1);
});


module.exports = app;