import { fetchData } from "../api/bvsData.js";

class BvsDataFormatter {

  async getAllSports(isoLanguage = "en-gb") {
    const sportsList = [];
    try {
      const sports = await fetchData(isoLanguage);
      this.sortOnPos(sports);
      sports.forEach((sport) =>
        sportsList.push({ id: sport.id, desc: sport.desc, pos: sport.pos })
      );
    } catch (err) {
      throw err;
    }
    return { language: isoLanguage, sports: sportsList };
  }

  async getAllSportsInMultiLang(languageIsoCodes) {
    const multiLangSportsList = [];
    try {
      for (const isoLanguage of languageIsoCodes) {
        let sportsList = await this.getAllSports(isoLanguage);
        multiLangSportsList.push(sportsList);
      }
    } catch (err) {
      throw err;
    }
    return multiLangSportsList;
  }

  async getAllEvents(sportId) {
    try {
      let sports = await fetchData();

      if (sportId) {
        sports = sports.filter((sport) => sportId === sport.id);
      }

      const eventsPerSports = this.getEventsPerSports(sports);
      return this.sortOnPos(eventsPerSports);
    } catch (err) {
      throw err;
    }
  }

  async showEventData(eventId) {
    try {
      const sports = await fetchData();
      const events = this.reduceSports(sports, eventId);
      const event = events.filter((event) => eventId === event.id);
      return event.length !== 0 ? event : `Data not found with id: ${eventId}`;
    } catch (err) {
      throw err;
    }
  }

  getEventsPerSports(sports) {
    return sports.reduce(
      (acc, sport) => [
        ...acc,
        {
          sport: sport.desc,
          sportId: sport.id,
          pos: sport.pos,
          events: { ...this.reduceCompetitions(sport.comp) },
        },
      ],
      []
    );
  }

  reduceCompetitions(competitions, eventId) {
    const events = competitions.reduce(
      (acc, comp) => [...acc, ...comp.events],
      []
    );

    if (!eventId) {
      return events.map((event) => {
        return {
          id: event.id,
          desc: event.desc,
          event_type: event.event_type,
          time: event.time,
        };
      });
    }

    return events;
  }

  reduceSports(sports, eventId) {
    return sports.reduce(
      (acc, sport) => [...acc, ...this.reduceCompetitions(sport.comp, eventId)],
      []
    );
  }

  sortOnPos(list) {
    return list.sort((a, b) => a.pos - b.pos);
  }
}

module.exports = BvsDataFormatter;
