import DataFormatter from '../services/BvsDataFormatter';
import i18n from "i18n";

const dataFormatter = new DataFormatter();

/*
 * GET /sports list all sports 
 */
async function listAllSports(req, res, next) {
  try {
    const sportsList = await dataFormatter.getAllSports();
    res.json(sportsList);
  } catch (error) {
    next(error)
  }
}

/*
 * GET /sports/:sportId/events list all events(per sportId) – where sportId is optional parameter.
 */
async function listEvents(req, res, next) {
  try {
    const sportId = parseInt(req.params.sportId,10);
    const eventsList = await dataFormatter.getAllEvents(sportId);
    res.json(eventsList);
  } catch (error) {
    next(error)
  }
}

/*
 * GET /events/:eventId list all data for a given event
 */
async function showEventData(req, res, next) {
  try {
    const eventId = parseInt(req.params.eventId,10);
    const eventData = await dataFormatter.showEventData(eventId);
    res.json(eventData);
  } catch (error) {
    next(error)
  }
}

/*
 * GET /sports/allLanguages list all sports in all languages
 */
async function listAllSportsMultiLang(req, res, next) {
  const languageIsoCodes = i18n.getLocales();
    try {
      const multiLangSportsList = await dataFormatter.getAllSportsInMultiLang(languageIsoCodes)
      res.json(multiLangSportsList);
    } catch (error) {
      next(error)
    }
  }

module.exports = {
  listAllSports,
  listEvents,
  showEventData,
  listAllSportsMultiLang
};