import axios from "axios";

export const fetchData = async (isoLanguage='en-gb') => {
  try {
    const url = `https://partners.betvictor.mobi/${isoLanguage}/in-play/1/events`;
    const fetchedData = await axios.get(url);

    return fetchedData.data.result.sports;
  } catch (error) {
    console.log(error);
  }
};

export const fetchIdForTest = async () => {
  try {
    const fetchedData = await fetchData()
    return fetchedData[0].comp[0].events[0].id;
  } catch (error) {
    console.log(error);
  }
};
