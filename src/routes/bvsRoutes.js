import bvsController from '../controllers/bvsController.js';

const routes = (app) => {

    app.route('/sports')
    .get(bvsController.listAllSports);
    
    app.route('/sports/:sportId?/events')
    .get(bvsController.listEvents);

    app.route('/events/:eventId')
    .get(bvsController.showEventData);

    app.route('/sports/allLanguages')
    .get(bvsController.listAllSportsMultiLang);
}

export default routes;