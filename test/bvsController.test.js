import "babel-polyfill";
import bvsController from "../src/controllers/bvsController";
import {fetchIdForTest} from '../src/api/bvsData'
import i18n from "i18n";

import chai from "chai";
import chaiHttp from "chai-http";
import router from "../src/routes/bvsRoutes";
import server from "../server";
const should = chai.should();

chai.use(chaiHttp);
chai.use(require("chai-json"));
describe("bvsController", () => {
  describe("/GET listAllSports", () => {
    it("it should GET all the sports", (done) => {
      chai
        .request(server)
        .get("/sports")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.not.have.property("errors");
          res.body.should.have.property("sports");
          done();
        });
    });
  });

  describe("/GET listEvents", () => {
    let sportId;
    it("it should GET all events for multiple sports called without sportId", (done) => {
      chai
        .request(server)
        .get("/sports/" + sportId + "/events")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.should.not.have.property("errors");
          done();
        });
    });
    it("it should GET only events of the specified sport when called with sportId", (done) => {
      sportId = 100;
      chai
        .request(server)
        .get("/sports/" + sportId + "/events")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.length.should.be.eql(1);
          res.body[0].should.have.property("events");
          res.body.should.not.have.property("errors");
          done();
        });
    });
  });

  describe("/GET showEventData", () => {
    let eventId;
    it("it should GET all details of the specified event", async () => {
      eventId = await fetchIdForTest();
        chai
          .request(server)
          .get("/events/" + eventId)
          .end((err, res) => {
            try {
              res.should.have.status(200);
              res.body.should.be.a("array");
              res.body.length.should.be.eql(1);
              res.body[0].should.have.property("event_type");
              res.body.should.not.have.property("errors");
            } catch (error) {
              console.log(error);
            }
      
          });
   
    });
    it("it should GET a message if there is no  event witrh the specified eventId", (done) => {
      eventId = "xx33x33x";
      chai
        .request(server)
        .get("/events/" + eventId)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("string");
          res.body.should.not.have
            .property("message")
            .eql(`Data not found with id: ${eventId}`);
          done();
        });
    });
  });

  describe("/GET listAllSportsMultiLang", () => {
    const languageIsoCodes = i18n.getLocales();
    it("it should GET all the sports in every supported language", (done) => {
      chai
        .request(server)
        .get("/sports/allLanguages")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.length.should.be.eql(languageIsoCodes.length);
          res.body.should.not.have.property("errors");
          for (const list of res.body) {
            list.should.have.property("language");
          }
          done();
        });
    });
  });
});
